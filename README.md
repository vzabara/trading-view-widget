## Trading View Financial Chart Widget for phpBB ##

The widget allows to show Trading View Financial Chart Widget on top of forum topics.

![ACP](screenshot-3.png)

![ACP](screenshot-2.png)

![Topic](screenshot-1.png)
See https://www.tradingview.com/widget/advanced-chart/
