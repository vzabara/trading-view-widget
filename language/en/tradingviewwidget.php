<?php

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'TRADINGVIEW_PAGE'              => 'Stock Analytics Widget',
	'ACP_TRADINGVIEW_TITLE'         => 'Stock Analytics Widget',
	'ACP_TRADINGVIEW_EXPLAIN'       => '',
	'ACP_TRADINGVIEW_WIDGETS'       => 'Stock Analytics Widgets',
	'ACP_TRADINGVIEW_SETTINGS'      => 'Stock Analytics Widget',
	'ACP_TRADINGVIEW_SAVED'         => 'Record have been saved successfully!',
	'EDIT_WIDGET'                  => 'Edit Widget',
	'ADD_WIDGET'                   => 'Add Widget',
	'NO_WIDGET'                    => 'Stock Analytics widget was not found',
	'ACP_NO_WIDGETS'               => 'Stock Analytics widget was not found',
	'FORM_INVALID'                 => 'Something wrong with form data',
	'ENTER_DATA'                   => 'Data was not set',
	'WIDGET_UPDATED'               => 'Stock Analytics widget was updated',
	'WIDGET_ADDED'                 => 'Stock Analytics widget was added',
	'WIDGET_REMOVED'               => 'Stock Analytics widget was removed',
	'FORUM_TITLE'                  => 'Forum title',
	'TOPIC_TITLE'                  => 'Topic title',
	'SYMBOL_TITLE'                 => 'Symbol',
));