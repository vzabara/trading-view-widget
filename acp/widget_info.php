<?php

namespace athc\tradingviewwidget\acp;

class widget_info
{
	public function module()
	{
		return array(
			'filename'  => '\athc\tradingviewwidget\acp\widget_module',
			'title'     => 'ACP_TRADINGVIEW_TITLE',
			'modes'    => array(
				'settings'  => array(
					'title' => 'ACP_TRADINGVIEW_SETTINGS',
					'auth'  => 'ext_athc/tradingviewwidget && acl_a_board',
					'cat'   => array('ACP_TRADINGVIEW_TITLE'),
				),
			),
		);
	}
}